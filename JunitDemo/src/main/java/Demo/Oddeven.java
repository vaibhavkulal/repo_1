package Demo;

import java.util.HashMap;
import java.util.Map;
public class Oddeven {

	
	public boolean check(int n) {
		if(n%2==0) {
			return true;
		}else {
			return false;
		}
	}
	
	public String getCapital(String country){
        
		
		Map<String,String> countryCapitalMap = new HashMap<String,String>();
        countryCapitalMap.put("Italy", "Rome");
        countryCapitalMap.put("Japan", "Tokyo");
        countryCapitalMap.put("Zimbabwe", "Harare");
        countryCapitalMap.put("Belgiaum", "Brussels");
        return countryCapitalMap.get(country);
    }
}
