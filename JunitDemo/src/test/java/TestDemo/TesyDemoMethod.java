package TestDemo;

import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Demo.Oddeven;

public class TesyDemoMethod {
	
	static Oddeven obj;

@BeforeAll
public static void abc1() {
	 obj=new Oddeven();
	System.out.println("I am BeforeAll");
}

@BeforeEach
public void abc2() {
	System.out.println("I am BeforeEach");
}


@AfterEach
public void abc4() {
	System.out.println("I am AfterEach");
}

@AfterAll
public static void abc5() {
	System.out.println("I am AfterAll");
}


@Test
public void DemoTest() {
	obj=new Oddeven();
	String capitalIndia=obj.getCapital("India");
	

	assertTrue(obj.check(8));
	assertFalse(obj.check(9));

	Assertions.assertNull(capitalIndia);
	
}


@Test
public void testbhutan() {
	String capitalIndia=obj.getCapital("Italy");
	Assertions.assertNotNull(capitalIndia);
}
	
}
